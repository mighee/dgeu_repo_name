import React from 'react';
import AppDatosAbiertosContenedor from './containers/AppDatosAbiertosContenedor';
import { Provider } from 'react-redux';
import { store } from './state/store';
//import AppDatosAbiertosComponente from './componentsStyles/AppDatosAbiertosComponente';
//import AppDatosAbiertosContenedor from './containers/AppDatosAbiertosContenedor';
//import AppDatosAbiertosComponenteStyled from './componentsStyles/AppDatosAbiertosComponente';
//import AppDatosAbiertosComponenteStyled from './componentsStyles/AppDatosAbiertosComponenteStyled-ts';


function App() {
  return (
    <Provider store={store}>
      <div>
        <div id="cabecera">
          <div className="container">
            <div id="logo" className="od-logo">
              <h1 className="d-flex align-items-center">
                <a href="https://www.juntadeandalucia.es/index.html" className="logotipo">
                  <img alt="Portal de la Junta de Andalucía" src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo-junta-od.png" />
                </a>
                <a className="sublogo datosabiertos" href="https://www.juntadeandalucia.es/datosabiertos/portal.html" >
                  <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo-od.png" alt="Datos abiertos" />
                </a>
              </h1>
              <div className="clearlogo">&nbsp;</div>
              <div className="oculto">
                <ul>
                  <li>
                    <a title="Saltar el menú de navegación, e ir directamente a los contenidos del Portal Oficial de la Junta de Andalucía" href="#contenidos">Saltar al contenido del Portal</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="herramienta">
              <div id="ubicacion">
                <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/cabecera_ubicacion.gif" alt="Portal de la Junta de Andalucía" width="275" height="40" />
              </div>
            </div>
            <div className="buscador">
              <form target="_self" className="floatright" method="get" action="https://www.juntadeandalucia.es/buscar.html">
                <fieldset>
                  <legend>Buscador general</legend>
                  <label className="l_oculto">Buscar</label>
                  <input style={{ position: "relative", zIndex: 99999 }} title="Buscar" className="input_text defaultTextActive ui-autocomplete-input" type="text" name="busquedageneral" id="busquedaCabecera" value="Buscar" />
                  <input name="buscargeneral" className="input_submit" value="Buscar" type="submit" title="Buscar" alt="Buscar" />
                  <p>p. ej. <a href="https://www.juntadeandalucia.es/buscar.html?busquedageneral=ofertas+de+empleo&amp;buscar=Buscar" title="ofertas de empleo">ofertas de empleo</a>, <a href="https://www.juntadeandalucia.es/buscar.html?busquedageneral=ayudas&amp;buscar=Buscar" title="ayudas">ayudas</a>, <a href="https://www.juntadeandalucia.es/buscar.html?busquedageneral=escolarizaci%C3%B3n&amp;buscar=Buscar" title="escolarización">escolarización</a>...</p>
                </fieldset>
              </form>
            </div>
            <div className="clear"> </div>
          </div>
        </div>

        <div id="navegacion">
          <div id="menu_btn" className="on">
            <div className="img">&nbsp;</div>
            <span className="menutxt">Menú principal</span>
          </div>
          <ul id="menu" className="menu_principal main-nav-horizontal" style={{ display: "block" }}>
            <li className="li_inicio main-nav od_catalogo">
              <a title="Catálogo de datos" href="https://www.juntadeandalucia.es/datosabiertos/portal/catalogo.html" className="drop" >Catálogo de datos</a>
              <div className="dropdown container js" style={{ display: "none" }}>
                <div className="grid_6">
                  <p className="encabezado">
                    <strong>Buscar datos</strong>
                  </p>
                  <div className="buscador">
                    <form action="https://www.juntadeandalucia.es/datosabiertos/portal/catalogo.html" className="floatright" method="get" target="_self">
                      <fieldset>
                        <legend>Buscador general</legend>
                        <label className="l_oculto">Buscar</label>
                        <input alt="Buscar" className="input_text defaultTextActive" id="busquedaNavegacionCatalogoFijo" name="q" title="Buscar" type="text" value="Buscar" />
                        <input alt="Buscar" className="input_submit" title="Buscar" type="submit" value="Buscar" />
                      </fieldset>
                    </form>
                    <p>Utiliza este buscador para localizar cualquiera de los conjuntos de datos disponibles de la Junta de Andalucía.</p>
                  </div>
                </div>
                <div className="grid_10">
                  <p className="encabezado">
                    <strong>Últimos datos</strong>
                  </p>
                  <ul className="listado_resultados">
                    <li>
                      <span>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/vehiculos-propiedad-junta-de-andalucia">Vehículos propiedad de la Junta de Andalucía</a>
                      </span>
                      <span>
                        <a className="gris" title="ODS" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/vehiculos-propiedad-junta-de-andalucia#ODS">ODS</a>
                      </span>
                    </li>
                    <li>
                      <span>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/inmuebles-propiedad-de-la-junta-de-andalucia">Inmuebles propiedad de la Junta de Andalucía</a>
                      </span>
                      <span>
                        <a className="verde" title="ICMS" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/inmuebles-propiedad-de-la-junta-de-andalucia#ICMS">ICMS</a>
                        <a className="gris" title="OTROS" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/inmuebles-propiedad-de-la-junta-de-andalucia#OTROS">OTROS</a>
                        <a className="verde" title="RDF/TURTLE" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/inmuebles-propiedad-de-la-junta-de-andalucia#RDF/TURTLE">RDF/TURTLE</a>
                        <a className="gris" title="JSON" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/inmuebles-propiedad-de-la-junta-de-andalucia#JSON">JSON</a>
                        <a className="gris" title="ODS" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/inmuebles-propiedad-de-la-junta-de-andalucia#ODS">ODS</a>
                      </span>
                    </li>
                    <li>
                      <span>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/relacion-de-puestos-de-trabajo-de-la-junta-de-andalucia">Relación de puestos de trabajo de la Junta de Andalucía</a>
                      </span>
                      <span>
                        <a className="verde" title="ICMS" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/relacion-de-puestos-de-trabajo-de-la-junta-de-andalucia#ICMS">ICMS</a>
                        <a className="gris" title="OTROS" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/relacion-de-puestos-de-trabajo-de-la-junta-de-andalucia#OTROS">OTROS</a>
                        <a className="verde" title="RDF/TURTLE" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/relacion-de-puestos-de-trabajo-de-la-junta-de-andalucia#RDF/TURTLE">RDF/TURTLE</a>
                        <a className="gris" title="JSON" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/relacion-de-puestos-de-trabajo-de-la-junta-de-andalucia#JSON">JSON</a>
                        <a className="gris" title="XLS" href="https://www.juntadeandalucia.es/datosabiertos/portal/dataset/relacion-de-puestos-de-trabajo-de-la-junta-de-andalucia#XLS">XLS</a>
                      </span>
                    </li>
                  </ul>

                  <div className="grid_7 alpha">
                    <a className="enlace_menu_mas_blanco_grande" href="https://www.juntadeandalucia.es/datosabiertos/portal/catalogo.html">Datos</a>
                  </div>
                  <div className="grid_3 omega">
                    <a className="enlace_menu_rss_grande" href="https://www.juntadeandalucia.es/datosabiertos/portal/catalogo.xml">Suscríbete</a>
                  </div>
                </div>
              </div>
            </li>
            <li className="main-nav od_tutoriales">
              <a title="Tutoriales" href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales.html" className="drop" >Tutoriales</a>
              <div className="dropdown container js" style={{ display: "none" }}>
                <div className="grid_6">
                  <p className="encabezado">
                    <strong>Cómo trabajar con los datos</strong>
                  </p>
                  <p>Consulta nuestra guía de pasos sobre cómo elaborar un proyecto de datos.</p>
                  <ul className="ul_img">
                    <li>
                      <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/basicos/piensa.html" title="Piensa">
                        <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/piensa-ico.png" alt="" style={{ width: "38px", height: "38px" }} />
                        <span>Piensa</span>
                      </a>
                    </li>
                    <li>
                      <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/basicos/descubre.html" title="Descubre">
                        <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/descubre-ico.png" alt="" style={{ width: "38px", height: "38px" }} />
                        <span>Descubre</span>
                      </a>
                    </li>
                    <li>
                      <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/basicos/desarrolla.html" title="Desarrolla">
                        <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/desarrolla-ico.png" alt="" style={{ width: "38px", height: "38px" }} />
                        <span>Desarrolla</span>
                      </a>
                    </li>
                    <li>
                      <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/basicos/publica.html" title="Publica">
                        <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/publica-ico.png" alt="" style={{ width: "38px", height: "38px" }} />
                        <span>Publica</span>
                      </a>
                    </li>
                    <li>
                      <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/basicos/revisa.html" title="Revisa">
                        <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/revisa-ico.png" alt="" style={{ width: "38px", height: "38px" }} />
                        <span>Revisa</span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="grid_10">
                  <p className="encabezado">
                    <strong>Profundiza en tus conocimientos</strong>
                  </p>
                  <p>Si ya tienes conocimientos básicos, pero quieres
                  seguir profundizando, pueden interesarte nuestros tutoriales técnicos
                  ¡Consúltalos!
                  </p>

                  <div className="grid_4">
                    <ul className="ul_img">
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/visualizar-ckan.html" title="Visualizar datos dentro del Catálogo de Datos Abiertos">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/icono_ckan.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Visualizar datos dentro del Catálogo de Datos Abiertos</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/hojas-calculo.html" title="Trabajar con los datos en hojas de cálculo con Libreoffice Calc">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/ico_libreoffice_0.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Trabajar con los datos en hojas de cálculo con Libreoffice Calc</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/fusion-tables.html" title="Generar una visualización con Tablas Dinámicas de Google">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/ico_fusion_0.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Generar una visualización con Tablas Dinámicas de Google</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/notebook.html" title="Documentar el trabajo realizado con los datos con Notebooks">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/ico_jupyter.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Documentar el trabajo realizado con los datos con Notebooks</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="grid_4">
                    <ul className="ul_img">
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/tratar-pdf-tabula.html" title="Extraer tablas de datos de documentos PDF con Tabula">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/iconotabula_ok.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Extraer tablas de datos de documentos PDF con Tabula</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/usar-openrefine.html" title="Tratar conjuntos de datos con OpenRefine">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/openrefine.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Tratar conjuntos de datos con OpenRefine</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/tutorial-tableau.html" title="Analizar y visualizar conjuntos de datos con Tableau">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/icono_tableau.jpg" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Analizar y visualizar conjuntos de datos con Tableau</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/tutoriales/tecnicos/data-studio.html" title="Crear informes atractivos con Google Data Studio">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo_DataStudio.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Crear informes atractivos con Google Data Studio</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li className="main-nav od_aplicaciones">
              <a title="Aplicaciones" href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones.html" className="drop" >Aplicaciones</a>
              <div className="dropdown container js" style={{ display: "none" }}>
                <div className="grid_6">
                  <p className="encabezado">
                    <strong>Buscar aplicaciones</strong>
                  </p>
                  <div className="buscador">
                    <form action="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones.html" className="floatright" method="get" target="_self">
                      <fieldset>
                        <legend>Buscador de aplicaciones</legend>
                        <label className="l_oculto" >Buscar</label>
                        <input alt="Buscar" className="input_text defaultTextActive" id="busquedaNavegacionAplicaciones" name="busqueda" title="Buscar" type="text" value="Buscar" />
                        <input alt="Buscar" className="input_submit" title="Buscar" value="Buscar" type="submit" />
                      </fieldset>
                    </form>
                    <p>Utiliza este buscador para localizar aplicaciones realizadas a partir de datos abiertos. Explora y descubre sus posibilidades.</p>
                  </div>
                  <strong>
                    <a href="https://www.juntadeandalucia.es/datosabiertos/portal/contacto/sugiere.html" title="Ir a Sugiere una aplicación">Sugiere una aplicación</a>
                  </strong>
                  <p>¿Has creado una aplicación con los datos de la Junta de Andalucía? ¿Quieres compartirla? Cuéntanoslo.</p>
                </div>
                <div className="grid_10">
                  <p className="encabezado">
                    <strong>Aplicaciones destacadas</strong>
                  </p>
                  <div className="grid_4">
                    <ul className="ul_img">
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/140855.html" title="Ofertas de Empleo Público de la Junta de Andalucía">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/imagen_app_junta.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Ofertas de Empleo Público de la Junta de Andalucía</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/101669.html" title="RAIF Andalucía">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo_0.jpg" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>RAIF Andalucía</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/104632.html" title="BOJA Boletín Oficial Andalucía">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/bojalogo%25201.jpg" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>BOJA Boletín Oficial Andalucía</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/101675.html" title="Observatorio de Precios y Mercados">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logobservatorio.jpg" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Observatorio de Precios y Mercados</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="grid_4">
                    <ul className="ul_img">
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/101690.html" title="Perfil del Contratante">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo_perfildelcontratante.jpg" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Perfil del Contratante</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/132251.html" title="CDAU Downloader">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/cdau_simbologia_qgis2.jpg" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>CDAU Downloader</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/160167.html" title="Buscador de plazas de la RPT de la Junta de Andalucía">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/Buscador_RPT.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Buscador de plazas de la RPT de la Junta de Andalucía</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/159492.html" title="Geoportal de Agricultura, Pesca y Desarrollo Rural">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/geoportal270.png" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>Geoportal de Agricultura, Pesca y Desarrollo Rural</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/aplicaciones/detalle/101768.html" title="¿Dónde van mis impuestos?">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logoimpuestos.jpg" alt="" style={{ width: "38px", height: "38px" }} />
                          <span>¿Dónde van mis impuestos?</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li className="main-nav od_proyecto">
              <a title="Sobre el proyecto" href="https://www.juntadeandalucia.es/datosabiertos/portal/proyecto.html" className="drop" >Sobre el proyecto</a>
              <div className="dropdown container js" style={{ display: "none" }}>
                <div className="grid_5">
                  <strong>
                    <a title="Información general" href="https://www.juntadeandalucia.es/datosabiertos/portal/proyecto/informacion.html">Información general</a>
                  </strong>
                  <ul className="ul_simple">
                    <li>
                      <span>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/proyecto/informacion/objetivos.html">Objetivos</a>
                      </span>
                    </li>
                    <li>
                      <span>
                        <a href="https://www.juntadeandalucia.es/datosabiertos/portal/proyecto/informacion/incorporar.html">Cómo incorporar datos al catálogo</a>
                      </span>
                    </li>
                  </ul>
                </div>

                <div className="grid_6">
                  <strong>
                    <a title="Próximos pasos" href="https://www.juntadeandalucia.es/datosabiertos/portal/proyecto/proximos-pasos.html">Próximos pasos</a>
                  </strong>
                  <img alt="Próximos pasos" src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/datos_abiertos_proximos_pasos_solapa.jpg" />
                  <p>Tenemos un plan. Descubre qué vamos a hacer en los próximos meses.</p>
                </div>

                <div className="grid_5">
                  <strong>
                    <a title="Indicadores y estadísticas" href="https://www.juntadeandalucia.es/datosabiertos/portal/proyecto/indicadores.html">Indicadores y estadísticas</a>
                  </strong>
                  <p>
                    655 conjuntos de datos publicados</p>

                  <p>
                    92.36% de contenidos con licencia libre</p>

                  <p>
                    88.4% en formatos estructurados</p>

                  <p>
                    <a href="http://www.juntadeandalucia.es/datosabiertos/portal/proyecto/indicadores.html">Más información</a>
                  </p>
                </div>
              </div>
            </li>
            <li className="main-nav od_contacto">
              <a title="Contacto" href="https://www.juntadeandalucia.es/datosabiertos/portal/contacto.html" className="drop" >Contacto</a>
              <div className="dropdown container js" style={{ display: "none" }}>
                <div className="grid_5">
                  <strong>
                    <a title="Solicita un conjunto de datos" href="https://juntadeandalucia.es/datosabiertos/portal/contacto/consultas.html">Solicita un conjunto de datos</a>
                  </strong>
                  <img alt="Solicita un conjunto de datos" src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/datos_abiertos_solicita_solapa.jpg" />
                  <p>¿Hay algún conjunto de datos de la Junta de
                  Andalucía que quieres que publiquemos? Escríbenos y estudiaremos la
                      mejor manera de difundirla.</p>
                </div>

                <div className="grid_6">
                  <strong>
                    <a title="Sugiere una aplicación" href="https://www.juntadeandalucia.es/datosabiertos/portal/contacto/sugiere.html">Sugiere una aplicación</a>
                  </strong>
                  <img alt="Sugiere una aplicación" src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/datos_abiertos_sugiere_solapa.jpg" />
                  <p>¿Has creado una aplicación con los datos de la
                  Junta de Andalucía? ¿Tienes una idea para una aplicación pero no sabes
                                                cómo desarrollarla? Cuéntanoslo.</p>
                </div>

                <div className="grid_5">
                  <strong>
                    <a title="Consultas y sugerencias" href="https://www.juntadeandalucia.es/datosabiertos/portal/contacto/consultas.html">Consultas y sugerencias</a>
                  </strong>
                  <img alt="Consultas y sugerencias" src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/datos_abiertos_consultas_solapa.jpg" />
                  <p>¿Tienes alguna duda? ¿En qué crees que debemos mejorar? Ponte en contacto con nosotros.</p>
                </div>
              </div>
            </li>
          </ul>
        </div>

        <div id="miga">
          <div className="container js">
            <div className="texto">
              <span>
                <a href="https://www.juntadeandalucia.es/datosabiertos/portal.html" title="Datos abiertos">Datos abiertos</a>
              </span>
          &gt;
          <span>Aplicaciones</span>
            </div>
          </div>
        </div>

        <AppDatosAbiertosContenedor />

        <div id="pie">
          <div className="container pre-pie">
            <div className="grid_5 enlaces">
              <ul>
                <li>
                  <a href="https://www.juntadeandalucia.es/informacion/mapaweb.html" >Mapa del sitio</a>
                </li>
                <li>
                  <a href="https://www.juntadeandalucia.es/informacion/legal.html">Aviso legal</a>
                </li>
                <li>
                  <a href="https://www.juntadeandalucia.es/protecciondedatos.html">Protección de datos</a>
                </li>
                <li>
                  <a href="https://www.juntadeandalucia.es/informacion/lista.html">Lista de correos</a>
                </li>
                <li>
                  <a href="https://www.juntadeandalucia.es/informacion/fuentesweb.html">Fuentes web</a>
                </li>
                <li>
                  <a href="https://www.juntadeandalucia.es/informacion/contacto.html" >Contacto</a>
                </li>
              </ul>
            </div>
            <div className="grid_5 enlaces-accesibilidad">
              <ul>
                <li>
                  <a href="https://www.juntadeandalucia.es/informacion/accesibilidad.html" className="aa">Accesibilidad</a>
                </li>
                <li>
                  <a id="accesibleLink" href="https://accesible.juntadeandalucia.es/Volumes/Datos_26_dic_2019/29_dic_2019/DGEU/paginaJunta/Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones.html" className="universal">Activar herramientas de apoyo a la navegación</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="container">
            <div className="site-footer__bottom">
              <section className="row region region-footer-fifth">
                <div id="block-marcajuntadeandalucia" className="block block-blocks-terra block-blocks-terra-marca-junta-andalucia">
                  <div className="content">
                    <ul className="links--platforms">
                      <li>
                        <a href="https://www.juntadeandalucia.es/" rel="home" className="navbar-brand">
                          <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo-pie-junta.png" alt="Inicio" />
                        </a>
                      </li>
                      <li>
                        <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo-pie-40.png" alt="Logo 40 Aniversatio de la Junta de Andalucía" />
                      </li>
                      <li>
                        <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo-pie-magallanes.png" alt="Logo Andalucía, Origen y Destino" />
                      </li>
                      <li>
                        <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/agenda2030.png" alt="Objetivo de desarrollo sostenible agenda 2030" />
                      </li>
                    </ul>
                  </div>
                </div>
                <div id="block-socialmedialinks-2" className="block-social-media-links block block-social-media-links-block">
                  <div className="content">
                    <ul className="social-media-links--platforms">
                      <li>
                        <a href="https://www.twitter.com/andaluciajunta" aria-label="Twitter" title="Twitter">
                          <svg className="svg-inline--fa fa-twitter fa-w-16 fa-2x" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" viewBox="0 0 512 512" data-fa-i2svg="">
                            <path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
                          </svg>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.facebook.com/juntainforma" aria-label="Facebook" title="Facebook">
                          <svg className="svg-inline--fa fa-facebook-f fa-w-10 fa-2x" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" viewBox="0 0 320 512" data-fa-i2svg="">
                            <path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path>
                          </svg>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.youtube.com/channel/UCwoAou1VZfbYfz-TysRzDCA" aria-label="Youtube" title="Youtube">
                          <svg className="svg-inline--fa fa-youtube fa-w-18 fa-2x" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" role="img" viewBox="0 0 576 512" data-fa-i2svg="">
                            <path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path>
                          </svg>
                        </a>
                      </li>
                      <li>
                        <a href="https://www.instagram.com/andaluciajunta/" aria-label="Instagram" title="Instagram">
                          <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" className="svg-inline--fa fa-instagram fa-w-18 fa-2x" role="img" viewBox="0 0 448 512">
                            <path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
                          </svg>
                        </a>
                      </li>
                      <li>
                        <a href="https://t.me/AndaluciaJunta" aria-label="Telegram" title="Telegram">
                          <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="telegram-plane" className="svg-inline--fa fa-telegram-plane fa-w-18 fa-2x" role="img" viewBox="0 0 448 512">
                            <path fill="currentColor" d="M446.7 98.6l-67.6 318.8c-5.1 22.5-18.4 28.1-37.3 17.5l-103-75.9-49.7 47.8c-5.5 5.5-10.1 10.1-20.7 10.1l7.4-104.9 190.9-172.5c8.3-7.4-1.8-11.5-12.9-4.1L117.8 284 16.2 252.2c-22.1-6.9-22.5-22.1 4.6-32.7L418.2 66.4c18.4-6.9 34.5 4.1 28.5 32.2z"></path>
                          </svg>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </section>
            </div>
          </div>
          <script type="text/javascript">
            $("#accesibleLink")[0].href = "https://accesible.juntadeandalucia.es" + window.location.pathname;
            </script>
          <script type="text/javascript">
            urchinTracker();
            </script>

          <noscript>
            &amp;lt;iframe title="Google Tag Manager"
            src="https://www.googletagmanager.com/ns.html?id=GTM-WH72PV6"
            height="0"
            width="0"
            style="display:none;visibility:hidden"/&amp;gt;
         </noscript>

          <div> </div>
        </div>

        <ul className="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="ui-id-1" style={{ display: "none" }}><li>Sin resultados</li><li>Sin resultados</li></ul>

        <span role="status" aria-live="assertive" aria-relevant="additions" className="ui-helper-hidden-accessible"></span>



        <ul className="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="ui-id-1" style={{ display: "none" }}><li>Sin resultados</li></ul>
        <span role="status" aria-live="assertive" aria-relevant="additions" className="ui-helper-hidden-accessible"></span>


      </div>
    </Provider>

  );
}

export default App;
