import InterfaceEnumDatos from './InterfaceEnumDatos';


/*
    Estados globales
*/
interface IGlobalState {
    // FORMULARIO DE BUSQUEDA
    // Datos de la pagina web: informacion de las aplicaciones
    eg_objetos_datos_abiertos: Array<InterfaceEnumDatos>;
    // Para el combo select: app/web
    eg_comp_combo_select_app_web: string;
    eg_comp_combo_select_opciones_app_web: Array<string>;
    // Para el combo select: familias
    eg_comp_combo_select_familia: string;
    eg_comp_combo_select_opciones_familia: Array<string>;
    // Para el checkbox: favorito
    eg_comp_checkbox_favorito: boolean;
    // Para el nombre de todas las etiquetas
    eg_comp_nombre_todas_etiquetas: Array<string>;

    // PAGINACION
    // Para indicar el numero de item totales
    eg_comp_paginacion_num_res_totales: number;
    // Para indicar el numero de pagina actual
    eg_comp_paginacion_num_pagina_actual: number;
    // Numero de elementos por pagina
    eg_num_elementos_pagina: number;
    // Numero inicio de resultados
    eg_comp_paginacion_inicioResultado: number;
    // Numero final de resutlados
    eg_comp_paginacion_finalResultado: number;

    eg_comp_date_picker_fecha_inicio: Date;
    eg_comp_date_picker_fecha_fin: Date;

    // Extra redo/ undo
    eg_comp_paginacion_undo_redo_pasado: Array<{tipo: string, valor:any}>,
    eg_comp_pagination_undo_redo_presente: {tipo: string, valor:any},
    eg_comp_paginacion_undo_redo_futuro: Array<{tipo: string, valor:any}>,
    eg_comp_app_paginacion_canRedo: boolean,
    eg_comp_app_paginacion_canUndo: boolean


}

export default IGlobalState;





export const initialState: IGlobalState = {
    // num elementos por pagina
    eg_num_elementos_pagina: 3,
    // datos aplicacion
    eg_objetos_datos_abiertos: getDatos(),
    // datos combo 
    eg_comp_combo_select_app_web: 'Cualquiera',
    eg_comp_combo_select_opciones_app_web: ['Cualquiera', 'Web', 'Movil'],
    // datos combo 
    eg_comp_combo_select_familia: 'Cualquiera',
    eg_comp_combo_select_opciones_familia: ['Cualquiera', 'Sanidad', 'Naturaleza', 'Politica', 'Sociedad', 'Economia', 'Otros'],
    // todas las etiquetas
    eg_comp_nombre_todas_etiquetas: ['Publicada en Movil/Web', 'Familia', 'Aplicaciones destacadas', 'Fecha Public. Desde', 'Fecha Public. Hasta'],
    // opcion de apps favoritos desactivado
    eg_comp_checkbox_favorito: false,
    // numero de apps totales
    eg_comp_paginacion_num_res_totales: getDatos().length,
    // pagina actual
    eg_comp_paginacion_num_pagina_actual: 1,
    // Fecha desde buscar la fecha de publicacion
    eg_comp_date_picker_fecha_inicio: new Date(Date.UTC(2001,0,1)),
    // Fecha hasta buscar la fecha de publicacion
    eg_comp_date_picker_fecha_fin: new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(),0,0,0)),
    // Inicio y final de los resultados
    eg_comp_paginacion_inicioResultado:1,
    eg_comp_paginacion_finalResultado:3,
    
    // EXTRA UNDO - REDO
    eg_comp_pagination_undo_redo_presente: {tipo: '', valor:null},
    eg_comp_paginacion_undo_redo_pasado: new Array(),
    eg_comp_paginacion_undo_redo_futuro: new Array(),

    eg_comp_app_paginacion_canRedo: false,
    eg_comp_app_paginacion_canUndo: false
    
}

export function getSinDatos() {

    const items = [
        {
            id:0,
            titulo: 'SIN DATOS',
            srcImg: 'Junta de Andalucía - Datos abiertos Aplicaciones_files/noApp2.png',
            descripcion: 'SIN DATOS',
            autor: 'SIN DATOS',
            webMovil: [''],
            familia: [''],
            destacada:false,
            fechaPublic: '',
            fuente: '',
            precio: ''
        }
    ]

    return items;

}


export function getDatos() {

    const items = [
        {
            id:1,
            titulo: 'Sistema de información electoral',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/sistemaelectoral270_0_0.jpg',
            descripcion: ' Esta Web permite a cualquier usuario consultar los resultados electorales de las Elecciones al Parlamento de Andalucía, así como los referéndums de Iniciativa Autonómica y Estatuto Autonómico.',
            autor: 'Consejería de Justicia e Interior - Junta de Andalucía',
            webMovil: ['Cualquiera', 'Web'],
            familia: ['Cualquiera', 'Politica'],
            destacada:true,
            fechaPublic: '20/12/2016',
            fuente: 'BOJA excepto elecciones 1982',
            precio: 'Gratuita'
        },
        {
            id:2,
            titulo: 'TuMarea',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo_20.png',
            descripcion: 'Visualización de todas las tablas de mareas de los 68 puertos españoles con datos propios.',
            autor: 'Luis González',
            webMovil: ['Cualquiera', 'Web', 'Movil'],
            familia: ['Cualquiera', 'Naturaleza'],
            destacada:false,
            fechaPublic: '22/12/2016',
            fuente: 'Anuario de Mareas',
            precio: 'Gratuita'
        },
        {
            id:3,
            titulo: 'Ofertas de Empleo Público de la Junta de Andalucía',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/imagen_app_junta.png',
            descripcion: 'Información de las ofertas de empleo público de la Junta de Andalucía desde el año 1988.',
            autor: 'David Reyes López',
            webMovil: ['Cualquiera', 'Web'],
            familia: ['Cualquiera', 'Sociedad', 'Economia', 'Otros'],
            destacada:true,
            fechaPublic: '04/04/2018',
            fuente: 'Portal Datos Abiertos',
            precio: 'Gratuita'
        },
        {
            id:4,
            titulo: 'Guía Farmacológica',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/Icono-APP-Gu-a-Farmacologica-EPES-061.jpg',
            descripcion: 'Guía farmacológica especializada en la medicación utilizada en situaciones de urgencias y emergencias sanitarias.',
            autor: 'EPES',
            webMovil: ['Cualquiera', 'Web', 'Movil'],
            familia: ['Cualquiera', 'Sanidad'],
            destacada:false,
            fechaPublic: '20/12/2016',
            fuente: 'EPES',
            precio: 'Gratuita'
        },
        {
            id:5,
            titulo: 'iSéneca',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logoiseneca.png',
            descripcion: 'Gestión del alumnado, ausencias, seguimiento, etc... También gestiona la agenda del profesorado y la mensajería y avisos Séneca.',
            autor: 'Conserjería de Educación',
            webMovil: ['Cualquiera', 'Movil'],
            familia: ['Cualquiera', 'Sociedad', 'Politica' ,'Otros'],
            destacada:true,
            fechaPublic: '20/12/2001',
            fuente: 'Conserjería de Educación',
            precio: 'Gratuita'
        },
        {
            id:6,
            titulo: 'RAIF Andalucía',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo_0.jpg',
            descripcion: 'Información fitosanitaria de los principales cultivos de Andalucía. A través de un Visor SIG podrá ver geográficamente, cómo están afectando las plagas y/o enfermedades sobre esos cultivos.',
            autor: 'Conserjería de Agricultura',
            webMovil: ['Cualquiera', 'Movil'],
            familia: ['Cualquiera', 'Naturaleza',  'Politica', 'Otros'],
            destacada:true,
            fechaPublic: '20/12/2016',
            fuente: 'Conserjería de Agricultura',
            precio: 'Gratuita'
        },
        {
            id:7,
            titulo: 'Fuentes de Andalucía',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/logo.png',
            descripcion: 'Catálogo-inventario de los manantiales y fuentes de Andalucía',
            autor: 'Instituto de estadística',
            webMovil: ['Cualquiera', 'Web'],
            familia: ['Cualquiera', 'Naturaleza', 'Otros'],
            destacada:false,
            fechaPublic: '20/12/2016',
            fuente: 'Sistema Estadístico de Andalucía',
            precio: 'Gratuita'
        },
        {
            id:1,
            titulo: 'Plan Romero',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/planromero2019.png',
            descripcion: 'Descripción: APP para la consulta de toda la información de las hermandades, con datos de los itinerarios que recorren, lugares de paso, hermandades en peregrinación, y seguimiento en tiempo real a partir de dispositivos GPS.',
            autor: 'Conserjería de Justicia e Interior',
            webMovil: ['Cualquiera', 'Movil'],
            familia: ['Cualquiera', 'Sociedad', 'Otros'],
            destacada:false,
            fechaPublic: '20/12/2016',
            fuente: 'Conserjería de Justicia e Interior',
            precio: 'Gratuita'
        },
        {
            id:9,
            titulo: 'Consulta de Registro de partidos políticos',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/registro-partidos2.png',
            descripcion: 'Contiene la información sobre las diversas formaciones políticas.',
            autor: 'Ministerior del Interior',
            webMovil: ['Cualquiera', 'Web'],
            familia: ['Cualquiera', 'Politica'],
            destacada:false,
            fechaPublic: '20/12/2016',
            fuente: 'Ministerior del Interior',
            precio: 'Gratuita'
        },
        {
            id:10,
            titulo: 'iMar',
            srcImg: 'Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/2019-11-06_20h25_59.png',
            descripcion: 'Descripción: Aplicación que permite conocer el estado del mar: oleaje, nivel del mar, viento, presión atmosférica, temperatura del agua.',
            autor: 'Puertos del Estado',
            webMovil: ['Cualquiera', 'Web', 'Movil'],
            familia: ['Cualquiera', 'Naturaleza', 'Sociedad', 'Otros'],
            destacada:false,
            fechaPublic: '10/10/2001',
            fuente: 'Puertos del Estado',
            precio: 'Gratuita'
        }
    ];

    return items;

}