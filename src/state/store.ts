import { Action, createStore } from 'redux';
import IGlobalState, { initialState, getDatos, getSinDatos } from './globalState';
import { FormularioBusquedaAction, IFormularioBusquedaAction } from '../actions/FormularioBusquedaAction';
import { PaginacionAction, IPaginacionAction } from '../actions/PaginacionAction'
import { DatePickerAction, IDatePickerAction } from '../actions/DatePickerAction';
import DatePickerComponente from '../components/DatePickerComponente';


const reducer = (state: IGlobalState = initialState, action: Action) => {

  let auxData = getDatos();
  let numResultadosTotalesGlobalStateAux = 0;
  let numPage = 0;

  switch (action.type) {

    // EXTRA 
    case FormularioBusquedaAction.ACT_FB_RESET:

      // Boton reset resetea los valores iniciales necesarios para aplicar el undo/redo
      state.eg_comp_paginacion_undo_redo_futuro = [];
      state.eg_comp_paginacion_undo_redo_pasado = [];
      state.eg_comp_pagination_undo_redo_presente = {tipo: '', valor:null};
      state.eg_comp_app_paginacion_canRedo= false;
      state.eg_comp_app_paginacion_canUndo= false;


      return {
        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_res_totales: auxData.length,
        eg_comp_paginacion_num_pagina_actual: 1,
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? 1 : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 2 ? 3 : auxData.length
      }

    // caso de seleccinar un tipo el tipo de aplicacion
    case FormularioBusquedaAction.ACT_FB_SELECT_APP_WEB:

      const form_busq_action1 = action as IFormularioBusquedaAction;

      // filtro los objetos segun las opciones que ya estan selecciondas: combo familia y checkbox
      auxData = auxData.filter(dato => dato.familia.indexOf(state.eg_comp_combo_select_familia) > -1);
      if (state.eg_comp_checkbox_favorito) {

        auxData = auxData.filter(dato => dato.destacada === state.eg_comp_checkbox_favorito);
      }

      // filtro segun la nueva opcion seleccionada
      auxData = auxData.filter(dato => dato.webMovil.indexOf(form_busq_action1.act_fb_select_app_web) > -1)

      // Filtro fecha hasta si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_fin.getTime() != new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) <= state.eg_comp_date_picker_fecha_fin.getTime());
      }

      // Filtro fecha desde si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_inicio.getTime() != new Date(Date.UTC(2001, 0, 1)).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) >= state.eg_comp_date_picker_fecha_inicio.getTime());
      }

      // Numero de objetos segun los campos seleccionados
      numResultadosTotalesGlobalStateAux = auxData.length;
      // Muestro los resultados en la pagina 1
      auxData = auxData.slice(0, state.eg_num_elementos_pagina);

      // cuando no hay datos cargo el mensaje correspondiente
      if (auxData.length === 0) {

        auxData = getSinDatos();
      }

      // Para la funcion undo/redo
      if(state.eg_comp_pagination_undo_redo_presente.tipo != ''){

        state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: state.eg_comp_pagination_undo_redo_presente.tipo, 
                                                            valor:  state.eg_comp_pagination_undo_redo_presente.valor});
      
      }
        
      state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: FormularioBusquedaAction.ACT_FB_SELECT_APP_WEB, valor:  state.eg_comp_combo_select_app_web});
      
      return {
        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_res_totales: numResultadosTotalesGlobalStateAux,
        eg_comp_combo_select_app_web: form_busq_action1.act_fb_select_app_web,
        eg_comp_paginacion_num_pagina_actual: 1,
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? 1 : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 2 ? 3 : auxData.length,
        // EXTRA
        eg_comp_pagination_undo_redo_presente: { tipo: FormularioBusquedaAction.ACT_FB_SELECT_APP_WEB, valor: form_busq_action1.act_fb_select_app_web },
        eg_comp_app_paginacion_canUndo:true

      }

    // caso de seleccinar un tipo de familia
    case FormularioBusquedaAction.ACT_FB_SELECT_FAMILIA:

      const form_busq_action2 = action as IFormularioBusquedaAction;

      // filtro los objetos segun las opciones que ya estan selecciondas: combo app/web y checkbox
      auxData = auxData.filter(dato => dato.webMovil.indexOf(state.eg_comp_combo_select_app_web) > -1);

      if (state.eg_comp_checkbox_favorito) {
        auxData = auxData.filter(dato => dato.destacada === state.eg_comp_checkbox_favorito);
      }

      // filtro segun la nueva opcion seleccionada
      auxData = auxData.filter(dato => dato.familia.indexOf(form_busq_action2.act_fb_select_familia) > -1)

      // Filtro fecha hasta si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_fin.getTime() !== new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) <= state.eg_comp_date_picker_fecha_fin.getTime());
      }

      // Filtro fecha desde si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_inicio.getTime() !== new Date(Date.UTC(2001, 0, 1)).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) >= state.eg_comp_date_picker_fecha_inicio.getTime());
      }


      // Numero de objetos segun los campos seleccionados
      numResultadosTotalesGlobalStateAux = auxData.length;

      // Muestro los resultados en la pagina 1
      auxData = auxData.slice(0, state.eg_num_elementos_pagina);

      // cuando no hay datos cargo el mensaje correspondiente
      if (auxData.length === 0) {

        auxData = getSinDatos();
      }

      // Para la funcion undo/redo
      if(state.eg_comp_pagination_undo_redo_presente.tipo != ''){

        state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: state.eg_comp_pagination_undo_redo_presente.tipo, 
                                                            valor:  state.eg_comp_pagination_undo_redo_presente.valor});
      
      }
      state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: FormularioBusquedaAction.ACT_FB_SELECT_FAMILIA, valor:  state.eg_comp_combo_select_familia});
      
      
      

      return {
        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_res_totales: numResultadosTotalesGlobalStateAux,
        eg_comp_combo_select_familia: form_busq_action2.act_fb_select_familia,
        eg_comp_paginacion_num_pagina_actual: 1,
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? 1 : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 2 ? 3 : auxData.length,
        // EXTRA
        eg_comp_pagination_undo_redo_presente: { tipo: FormularioBusquedaAction.ACT_FB_SELECT_FAMILIA, valor: form_busq_action2.act_fb_select_familia },
        eg_comp_app_paginacion_canUndo:true
  
      }

    // caso de seleccionar o no el checkbox de favoritas 
    case FormularioBusquedaAction.ACT_FB_CHECKBOX_FAVORITAS:

      const form_busq_action3 = action as IFormularioBusquedaAction;

      // filtro los objetos segun las opciones que ya estan selecciondas: combo app/web y familias
      auxData = auxData.filter(dato => dato.webMovil.indexOf(state.eg_comp_combo_select_app_web) > -1);
      auxData = auxData.filter(dato => dato.familia.indexOf(state.eg_comp_combo_select_familia) > -1);

      // filtro segun la nueva opcion seleccionada
      if (form_busq_action3.act_fb_checkbox_favoritas) {

        auxData = auxData.filter(dato => dato.destacada === form_busq_action3.act_fb_checkbox_favoritas);
      }

      // Filtro fecha hasta si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_fin.getTime() != new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) <= state.eg_comp_date_picker_fecha_fin.getTime());
      }

      // Filtro fecha desde si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_inicio.getTime() != new Date(Date.UTC(2001, 0, 1)).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) >= state.eg_comp_date_picker_fecha_inicio.getTime());
      }


      // Numero de objetos segun los campos seleccionados
      numResultadosTotalesGlobalStateAux = auxData.length;

      numPage = state.eg_comp_paginacion_num_pagina_actual;

      auxData = auxData.slice(0, state.eg_num_elementos_pagina);

      // cuando no hay datos cargo el mensaje correspondiente
      if (auxData.length === 0) {

        auxData = getSinDatos();
      }

      // Para la funcion undo/redo
      if(state.eg_comp_pagination_undo_redo_presente.tipo != ''){

        state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: state.eg_comp_pagination_undo_redo_presente.tipo, 
                                                            valor:  state.eg_comp_pagination_undo_redo_presente.valor});
      
      }

      state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: FormularioBusquedaAction.ACT_FB_CHECKBOX_FAVORITAS, valor:  state.eg_comp_checkbox_favorito});
      

      return {
        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_res_totales: numResultadosTotalesGlobalStateAux,
        eg_comp_checkbox_favorito: form_busq_action3.act_fb_checkbox_favoritas,
        eg_comp_paginacion_num_pagina_actual: 1,
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? 1 : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 2 ? 3 : auxData.length,
        // EXTRA
        //eg_comp_paginacion_undo_redo_pasado: [...state.eg_comp_paginacion_undo_redo_pasado,
        eg_comp_pagination_undo_redo_presente: { tipo: FormularioBusquedaAction.ACT_FB_CHECKBOX_FAVORITAS, valor: form_busq_action3.act_fb_checkbox_favoritas },
        eg_comp_app_paginacion_canUndo:true
      }

    // Caso de actualizar la pagina
    case PaginacionAction.ACT_PAG_UPDATE_PAGE:

      const paginacionAction4 = action as IPaginacionAction;

      // filtro los objetos segun las opciones que ya estan selecciondas: combo app/web, familias y checkbox
      auxData = auxData.filter(dato => dato.webMovil.indexOf(state.eg_comp_combo_select_app_web) > -1);
      auxData = auxData.filter(dato => dato.familia.indexOf(state.eg_comp_combo_select_familia) > -1);

      if (state.eg_comp_checkbox_favorito) {

        auxData = auxData.filter(dato => dato.destacada === state.eg_comp_checkbox_favorito);
      }

      // Filtro fecha hasta si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_fin.getTime() != new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) <= state.eg_comp_date_picker_fecha_fin.getTime());
      }

      // Filtro fecha desde si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_inicio.getTime() != new Date(Date.UTC(2001, 0, 1)).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) >= state.eg_comp_date_picker_fecha_inicio.getTime());
      }


      // Numero de objetos segun los campos seleccionados
      numResultadosTotalesGlobalStateAux = auxData.length;


      // filtro segun la nueva opcion seleccionada: pagina 
      numPage = paginacionAction4.act_paginacion_pagina_actual;

      // Indica pagina 0 primera
      if (numPage === -10) {

        auxData = auxData.slice(0, state.eg_num_elementos_pagina);
        numPage = 1;

        // Indica avanza una pagina
      } else if (numPage === 0) {

        // Si no nos encontramos en la ultima pagina
        if (state.eg_comp_paginacion_num_pagina_actual < Math.ceil(numResultadosTotalesGlobalStateAux / state.eg_num_elementos_pagina) - 1) {

          auxData = auxData.slice((state.eg_comp_paginacion_num_pagina_actual) * state.eg_num_elementos_pagina, (state.eg_comp_paginacion_num_pagina_actual + 1) * state.eg_num_elementos_pagina);
          numPage = state.eg_comp_paginacion_num_pagina_actual + 1;

          // Si nos encontramos en la ultima pagina con resultados y queremos avanzar una pagina mas
        } else {

          auxData = auxData.slice(auxData.length - ((auxData.length % state.eg_num_elementos_pagina > 0 ? auxData.length % state.eg_num_elementos_pagina : state.eg_num_elementos_pagina)), auxData.length);
          numPage = Math.ceil(numResultadosTotalesGlobalStateAux / state.eg_num_elementos_pagina);
        }

        // Indica retrocede una pagina
      } else if (numPage === -1) {

        // Si estamos en cualquier pagina que no sea la primera
        if (state.eg_comp_paginacion_num_pagina_actual > 1) {

          auxData = auxData.slice((state.eg_comp_paginacion_num_pagina_actual - 2) * state.eg_num_elementos_pagina, (state.eg_comp_paginacion_num_pagina_actual - 1) * state.eg_num_elementos_pagina);
          numPage = state.eg_comp_paginacion_num_pagina_actual - 1;

          // Si nos encontramos en la primera pagina con resultados y queremos retroceder una pagina mas
        } else {

          auxData = auxData.slice(0, state.eg_num_elementos_pagina);
          numPage = 1;
        }

        // Indica pagina final
      } else if (numPage === 10) {

        auxData = auxData.slice(auxData.length - ((auxData.length % state.eg_num_elementos_pagina > 0 ? auxData.length % state.eg_num_elementos_pagina : state.eg_num_elementos_pagina)), auxData.length);
        numPage = Math.ceil(numResultadosTotalesGlobalStateAux / state.eg_num_elementos_pagina);

      } else {

        // Cuando nos indican la pagina concreta
        let numPageMax = Math.ceil(numResultadosTotalesGlobalStateAux / state.eg_num_elementos_pagina)

        // Si la pagina que queremos cargar es menor que la maxima pagina con resultados
        if (numPage <= numPageMax) {

          // Cargamos los datos de la pagina
          auxData = auxData.slice((numPage - 1) * state.eg_num_elementos_pagina, (numPage) * state.eg_num_elementos_pagina);
        } else {

          // Si no podemos cargar la pagina mostramos la ultima pagina con resultados, independientemente de la
          // paquina que se indique.
          auxData = auxData.slice((numPageMax - 1) * state.eg_num_elementos_pagina, (numPageMax) * state.eg_num_elementos_pagina);
          numPage = numPageMax;
        }
      }

      // cuando no hay datos cargo el mensaje correspondiente
      if (auxData.length === 0) {

        auxData = getSinDatos();
      }

      return {
        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_res_totales: numResultadosTotalesGlobalStateAux,
        eg_comp_paginacion_num_pagina_actual: numPage,
        // Muestro los datos correspoiendes a la pagina cargada
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? ((numPage - 1) * 3 + 1) : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 0 ? ((numPage - 1) * 3 + 1) + (auxData.length > 2 ? 2 : auxData.length - 1) : auxData.length
      }

    case DatePickerAction.ACT_DATE_PICKER_FECHA_INICIO:

      const datePicker1 = action as IDatePickerAction;

      // filtro los objetos segun las opciones que ya estan selecciondas: combo app/web, familias y checkbox
      auxData = auxData.filter(dato => dato.webMovil.indexOf(state.eg_comp_combo_select_app_web) > -1);
      auxData = auxData.filter(dato => dato.familia.indexOf(state.eg_comp_combo_select_familia) > -1);


      if (state.eg_comp_checkbox_favorito) {

        auxData = auxData.filter(dato => dato.destacada === state.eg_comp_checkbox_favorito);
      }

      // Filtro fecha publicacion desde
      auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0])))).getTime() >= datePicker1.act_date_picker_fecha_inicio.getTime());

      // Filtro fecha hasta si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_fin.getTime() != new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) <= state.eg_comp_date_picker_fecha_fin.getTime());
      }

      // Numero de objetos segun los campos seleccionados
      numResultadosTotalesGlobalStateAux = auxData.length;

      // cuando no hay datos cargo el mensaje correspondiente
      if (auxData.length === 0) {

        auxData = getSinDatos();
      }

      
      // Para hacer undo al estado inicial

      if(state.eg_comp_pagination_undo_redo_presente.tipo != ''){

        state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: state.eg_comp_pagination_undo_redo_presente.tipo, 
                                                            valor:  state.eg_comp_pagination_undo_redo_presente.valor});
      
      }

      state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: DatePickerAction.ACT_DATE_PICKER_FECHA_INICIO, valor:  state.eg_comp_date_picker_fecha_inicio});
      

      

      return {
        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_res_totales: numResultadosTotalesGlobalStateAux,
        eg_comp_date_picker_fecha_inicio: datePicker1.act_date_picker_fecha_inicio,
        eg_comp_paginacion_num_pagina_actual: 1,
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? 1 : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 2 ? 3 : auxData.length,
        // EXTRA
        eg_comp_pagination_undo_redo_presente: { tipo: DatePickerAction.ACT_DATE_PICKER_FECHA_INICIO, valor: datePicker1.act_date_picker_fecha_inicio },
        eg_comp_app_paginacion_canUndo:true
        

      }

    case DatePickerAction.ACT_DATE_PICKER_FECHA_FIN:

      const datePicker2 = action as IDatePickerAction;

      // filtro los objetos segun las opciones que ya estan selecciondas: combo app/web, familias y checkbox
      auxData = auxData.filter(dato => dato.webMovil.indexOf(state.eg_comp_combo_select_app_web) > -1);
      auxData = auxData.filter(dato => dato.familia.indexOf(state.eg_comp_combo_select_familia) > -1);

      if (state.eg_comp_checkbox_favorito) {

        auxData = auxData.filter(dato => dato.destacada === state.eg_comp_checkbox_favorito);
      }


      // Filtro fecha publicacion hasta
      auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0])))).getTime() <= datePicker2.act_date_picker_fecha_fin.getTime());

      // Filtro fecha desde si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_inicio.getTime() !== new Date(Date.UTC(2001, 0, 1)).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0])))).getTime() >= state.eg_comp_date_picker_fecha_inicio.getTime());
      }


      // Numero de objetos segun los campos seleccionados
      numResultadosTotalesGlobalStateAux = auxData.length;

      // cuando no hay datos cargo el mensaje correspondiente
      if (auxData.length === 0) {

        auxData = getSinDatos();
      }

      
      // Para hacer undo al estado inicial

      if(state.eg_comp_pagination_undo_redo_presente.tipo != ''){

        state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: state.eg_comp_pagination_undo_redo_presente.tipo, 
                                                            valor:  state.eg_comp_pagination_undo_redo_presente.valor});
      
      }

      state.eg_comp_paginacion_undo_redo_pasado.push({ tipo: DatePickerAction.ACT_DATE_PICKER_FECHA_FIN, valor:  state.eg_comp_date_picker_fecha_fin});
      



      return {

        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_res_totales: numResultadosTotalesGlobalStateAux,
        eg_comp_date_picker_fecha_fin: datePicker2.act_date_picker_fecha_fin,
        eg_comp_paginacion_num_pagina_actual: 1,
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? 1 : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 2 ? 3 : auxData.length,
        // EXTRA
        //eg_comp_paginacion_undo_redo_pasado: [...state.eg_comp_paginacion_undo_redo_pasado,
        //  { tipo: DatePickerAction.ACT_DATE_PICKER_FECHA_FIN, valor: datePicker2.act_date_picker_fecha_fin }],
        eg_comp_pagination_undo_redo_presente: { tipo: DatePickerAction.ACT_DATE_PICKER_FECHA_FIN, valor: datePicker2.act_date_picker_fecha_fin },
        eg_comp_app_paginacion_canUndo:true
      }

    case PaginacionAction.ACT_PAG_UNDO_ACTION:
      
       
      var itemUNDO = state.eg_comp_paginacion_undo_redo_pasado.pop();
      state.eg_comp_paginacion_undo_redo_futuro.push(state.eg_comp_pagination_undo_redo_presente);
      state.eg_comp_app_paginacion_canRedo = true;


      if(itemUNDO !== undefined){

        
        if (state.eg_comp_pagination_undo_redo_presente.tipo === FormularioBusquedaAction.ACT_FB_SELECT_APP_WEB) {

          state.eg_comp_combo_select_app_web = itemUNDO.valor;

        }else if(state.eg_comp_pagination_undo_redo_presente.tipo === FormularioBusquedaAction.ACT_FB_SELECT_FAMILIA){

          state.eg_comp_combo_select_familia = itemUNDO.valor;

        }else if(state.eg_comp_pagination_undo_redo_presente.tipo === FormularioBusquedaAction.ACT_FB_CHECKBOX_FAVORITAS){

          state.eg_comp_checkbox_favorito = itemUNDO.valor;

        }else if(state.eg_comp_pagination_undo_redo_presente.tipo === DatePickerAction.ACT_DATE_PICKER_FECHA_INICIO){

          state.eg_comp_date_picker_fecha_inicio = itemUNDO.valor;

        }else if(state.eg_comp_pagination_undo_redo_presente.tipo === DatePickerAction.ACT_DATE_PICKER_FECHA_FIN){

          state.eg_comp_date_picker_fecha_fin = itemUNDO.valor;
        }

        let nuevoStadoActual = state.eg_comp_paginacion_undo_redo_pasado.pop();

        if(nuevoStadoActual != undefined){
            state.eg_comp_pagination_undo_redo_presente = nuevoStadoActual;
        }

        if(state.eg_comp_paginacion_undo_redo_pasado.length === 0){

          state.eg_comp_app_paginacion_canUndo = false;
        }

      }
    
    

      auxData = auxData.filter(dato => dato.webMovil.indexOf(state.eg_comp_combo_select_app_web) > -1);
      auxData = auxData.filter(dato => dato.familia.indexOf(state.eg_comp_combo_select_familia) > -1);

      if (state.eg_comp_checkbox_favorito) {

        auxData = auxData.filter(dato => dato.destacada === state.eg_comp_checkbox_favorito);
      }

      // Filtro fecha hasta si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_fin.getTime() !== new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth()-1, new Date().getDate())).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) <= state.eg_comp_date_picker_fecha_fin.getTime());
      }

      // Filtro fecha desde si ha cambiado de su valor por defecto
      if(state.eg_comp_date_picker_fecha_inicio.getTime() !== new Date(Date.UTC(2001, 0, 1)).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0])))).getTime() >= state.eg_comp_date_picker_fecha_inicio.getTime());
      }

      // Numero de objetos segun los campos seleccionados
      numResultadosTotalesGlobalStateAux = auxData.length;

      numPage = state.eg_comp_paginacion_num_pagina_actual;

      auxData = auxData.slice(0, state.eg_num_elementos_pagina);

      // cuando no hay datos cargo el mensaje correspondiente
      if (auxData.length === 0) {

        auxData = getSinDatos();
      }


      return {
        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_pagina_actual: 1,
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? 1 : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 2 ? 3 : auxData.length,
        eg_comp_paginacion_num_res_totales: numResultadosTotalesGlobalStateAux
      }

    case PaginacionAction.ACT_PAG_REDO_ACTION:

      var itemREDO = state.eg_comp_paginacion_undo_redo_futuro.pop();
      state.eg_comp_app_paginacion_canUndo = true;

      state.eg_comp_paginacion_undo_redo_pasado.push(state.eg_comp_pagination_undo_redo_presente);
      

      if(itemREDO !== undefined){

        
        if (itemREDO.tipo === FormularioBusquedaAction.ACT_FB_SELECT_APP_WEB) {

          state.eg_comp_paginacion_undo_redo_pasado.push({tipo:itemREDO.tipo, valor:state.eg_comp_combo_select_app_web});
          state.eg_comp_combo_select_app_web = itemREDO.valor;
         
        }else if(itemREDO.tipo === FormularioBusquedaAction.ACT_FB_SELECT_FAMILIA){

          
          state.eg_comp_paginacion_undo_redo_pasado.push({tipo:itemREDO.tipo, valor:state.eg_comp_combo_select_familia});
          state.eg_comp_combo_select_familia = itemREDO.valor;          

        }else if(itemREDO.tipo === FormularioBusquedaAction.ACT_FB_CHECKBOX_FAVORITAS){

          state.eg_comp_paginacion_undo_redo_pasado.push({tipo:itemREDO.tipo, valor:state.eg_comp_checkbox_favorito});
          state.eg_comp_checkbox_favorito = itemREDO.valor;
          
        }else if(itemREDO.tipo === DatePickerAction.ACT_DATE_PICKER_FECHA_INICIO){

          state.eg_comp_paginacion_undo_redo_pasado.push({tipo:itemREDO.tipo, valor:state.eg_comp_date_picker_fecha_inicio});
          state.eg_comp_date_picker_fecha_inicio = itemREDO.valor;

        }else if(itemREDO.tipo === DatePickerAction.ACT_DATE_PICKER_FECHA_FIN){

          state.eg_comp_paginacion_undo_redo_pasado.push({tipo:itemREDO.tipo, valor:state.eg_comp_date_picker_fecha_fin});
          state.eg_comp_date_picker_fecha_fin = itemREDO.valor;
        }
       
        state.eg_comp_pagination_undo_redo_presente = itemREDO;

        if(state.eg_comp_paginacion_undo_redo_futuro.length === 0){

          state.eg_comp_app_paginacion_canRedo = false;
        }

        state.eg_comp_pagination_undo_redo_presente = itemREDO;

        if(state.eg_comp_paginacion_undo_redo_futuro.length === 0){

          state.eg_comp_app_paginacion_canRedo = false;
        }

      }
      

      auxData = auxData.filter(dato => dato.webMovil.indexOf(state.eg_comp_combo_select_app_web) > -1);
      auxData = auxData.filter(dato => dato.familia.indexOf(state.eg_comp_combo_select_familia) > -1);

      if (state.eg_comp_checkbox_favorito) {

        auxData = auxData.filter(dato => dato.destacada === state.eg_comp_checkbox_favorito);
      }

      // Filtro fecha hasta si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_fin.getTime() != new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0]))).getTime()) <= state.eg_comp_date_picker_fecha_fin.getTime());
      }

      // Filtro fecha desde si ha cambiado de su valor por defecto
      if (state.eg_comp_date_picker_fecha_inicio.getTime() !== new Date(Date.UTC(2001, 0, 1)).getTime()) {

        auxData = auxData.filter(dato => (new Date(Date.UTC(Number(dato.fechaPublic.split("/")[2]), Number(dato.fechaPublic.split('/')[1]) - 1, Number(dato.fechaPublic.split('/')[0])))).getTime() >= state.eg_comp_date_picker_fecha_inicio.getTime());
      }

      // Numero de objetos segun los campos seleccionados
      numResultadosTotalesGlobalStateAux = auxData.length;

      numPage = state.eg_comp_paginacion_num_pagina_actual;

      auxData = auxData.slice(0, state.eg_num_elementos_pagina);

      // cuando no hay datos cargo el mensaje correspondiente
      if (auxData.length === 0) {

        auxData = getSinDatos();
      }


      return {
        ...state, eg_objetos_datos_abiertos: auxData,
        eg_comp_paginacion_num_pagina_actual: 1,
        eg_comp_paginacion_inicioResultado: auxData.length > 0 ? 1 : 0,
        eg_comp_paginacion_finalResultado: auxData.length > 2 ? 3 : auxData.length,
        eg_comp_paginacion_num_res_totales: numResultadosTotalesGlobalStateAux
      }


    default:
      return { ...state };

  }

  return state;
}

export const store = createStore(reducer, initialState);