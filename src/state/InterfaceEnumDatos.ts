interface InterfaceEnumDatos {

    id: number;
    titulo: string;
    srcImg: string;
    descripcion: string;
    autor: string;
    webMovil: Array<string>;
    familia: Array<string>;
    destacada: boolean;
    fechaPublic: string;
    fuente: string;
    precio: string;
}

export default InterfaceEnumDatos;