import { connect } from 'react-redux';
import AppDatosAbiertosComponente from '../components/AppDatosAbiertosComponente';
import IGlobalState from '../state/globalState';
import { Dispatch } from 'redux';
import comp_styles from '../componentsStyles/AppDatosAbiertosComponente-ts';
//import AppDatosAbiertosComponenteStyled from '../componentsStyles/AppDatosAbiertosComponente';





const mapStateToProps = (state: IGlobalState) => ({
    comp_lista_objetos: state.eg_objetos_datos_abiertos.slice(0,state.eg_num_elementos_pagina),
    comp_styles: comp_styles,
    comp_app_paginacion_canUndo: state.eg_comp_app_paginacion_canUndo,
    comp_app_paginacion_canRedo: state.eg_comp_app_paginacion_canRedo,

});



export default connect(mapStateToProps)(AppDatosAbiertosComponente);