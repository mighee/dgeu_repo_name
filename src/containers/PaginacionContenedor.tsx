import PaginacionComponente from '../components/PaginacionComponente';
import { connect } from 'react-redux';
import IGlobalState from '../state/globalState';
import { Dispatch } from 'redux';
import { PaginacionAction } from '../actions/PaginacionAction';

const mapStateToProps = (state: IGlobalState) => ({
    
    comp_paginacion_num_res_totales: state.eg_comp_paginacion_num_res_totales,
    comp_paginacion_num_pagina_actual: state.eg_comp_paginacion_num_pagina_actual,
    comp_paginacion_inicioResultado: state.eg_comp_paginacion_inicioResultado,
    comp_paginacion_finalResultado: state.eg_comp_paginacion_finalResultado
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    comp_paginacion_event_onClick: (event:number) => {
        dispatch({type: PaginacionAction.ACT_PAG_UPDATE_PAGE, act_paginacion_pagina_actual:event});
    },
    comp_paginacion_undo_event_onClick: () => {
        dispatch({type: PaginacionAction.ACT_PAG_UNDO_ACTION});
    },
    comp_paginacion_redo_event_onClick: () => {
        dispatch({type: PaginacionAction.ACT_PAG_REDO_ACTION});
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(PaginacionComponente);
