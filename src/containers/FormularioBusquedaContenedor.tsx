import FormularioBusquedaComponente from '../components/FormularioBusquedaComponente';
import { connect } from 'react-redux';
import IGlobalState from '../state/globalState';
import { Dispatch } from 'redux';
import { FormularioBusquedaAction } from '../actions/FormularioBusquedaAction';
import { DatePickerAction } from '../actions/DatePickerAction';

const mapStateToProps = (state: IGlobalState) => ({

    // Combo APP/WEB
    comp_combo_select_app_web: state.eg_comp_combo_select_app_web,
    comp_combo_select_opciones_app_web: state.eg_comp_combo_select_opciones_app_web,
    // Combo familia
    comp_combo_select_familia: state.eg_comp_combo_select_familia,
    comp_combo_select_opciones_familia: state.eg_comp_combo_select_opciones_familia,
    // CheckBox favoritas
    comp_checkbox_favorito:state.eg_comp_checkbox_favorito,
    // Lista etiquetas
    comp_nombre_todas_etiquetas: state.eg_comp_nombre_todas_etiquetas,
    // Fecha desde y hasta respest.
    comp_date_picker_fecha_inicio: state.eg_comp_date_picker_fecha_inicio,
    comp_date_picker_fecha_fin: state.eg_comp_date_picker_fecha_fin,
    
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    comp_combo_select_event_app_web: (event:string) => {
        dispatch({type: FormularioBusquedaAction.ACT_FB_SELECT_APP_WEB, act_fb_select_app_web: event});
    },
    comp_combo_select_event_familia: (event:string) => {
        dispatch({type: FormularioBusquedaAction.ACT_FB_SELECT_FAMILIA, act_fb_select_familia: event});
    },
    comp_checkbox_event_favorito: (event:boolean) => {
        dispatch({type: FormularioBusquedaAction.ACT_FB_CHECKBOX_FAVORITAS, act_fb_checkbox_favoritas: event});
    },
    comp_datepicker_event_fecha_ini:(event:Date) => {
        dispatch({type: DatePickerAction.ACT_DATE_PICKER_FECHA_INICIO, act_date_picker_fecha_inicio:event});
    },
    comp_datepicker_event_fecha_fin:(event:Date) => {
        dispatch({type: DatePickerAction.ACT_DATE_PICKER_FECHA_FIN, act_date_picker_fecha_fin:event});
    },
    comp_reset_undo_redo:() =>{
        dispatch({type: FormularioBusquedaAction.ACT_FB_RESET});
    }

})

export default connect(mapStateToProps, mapDispatchToProps)(FormularioBusquedaComponente);
