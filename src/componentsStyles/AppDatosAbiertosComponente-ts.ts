
export const comp_styles= {

    style_img_logo_fondo: {

        backgroundColor: "#222", 
        height: "100%"
    },

    style_p_font_size_titulo: {

        fontSize: '17px'
    },

    style_img_float_left:{

        float:'left' as 'left'
    },
    style_img_float_right:{

        float:'right' as 'right'
    },

    style_img_margin_auto: {

        margin: 'auto'
    },
    style_text_white: {
        
        color:'white'
    }

}

export default comp_styles;