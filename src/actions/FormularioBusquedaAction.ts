import { Action } from 'redux';

export enum FormularioBusquedaAction {

    ACT_FB_SELECT_APP_WEB = "ACT_FB_SELECT_APP_WEB",
    ACT_FB_SELECT_FAMILIA = "ACT_FB_SELECT_FAMILIA",
    ACT_FB_CHECKBOX_FAVORITAS = "ACT_FB_CHECKBOX_FAVORITAS",
    ACT_FB_RESET = "ACT_FB_RESET"
}

export interface IFormularioBusquedaAction extends Action {
    act_fb_select_app_web: string;
    act_fb_select_familia: string;
    act_fb_checkbox_favoritas: boolean;
}