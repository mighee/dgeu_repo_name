import { Action } from 'redux';

export enum DatePickerAction {
    ACT_DATE_PICKER_FECHA_INICIO = "ACT_DATE_PICKER_FECHA_INICIO",
    ACT_DATE_PICKER_FECHA_FIN = "ACT_DATE_PICKER_FECHA_FIN",
}

export interface IDatePickerAction extends Action {
    act_date_picker_fecha_inicio: Date;
    act_date_picker_fecha_fin: Date;

    
}