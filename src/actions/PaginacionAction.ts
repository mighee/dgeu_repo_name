import { Action } from 'redux';

export enum PaginacionAction {
    ACT_PAG_UPDATE_PAGE = "PAGINATION_UPDATE",
    ACT_PAG_UNDO_ACTION = "ACT_PAG_UNDO_ACTION",
    ACT_PAG_REDO_ACTION = "ACT_PAG_REDO_ACTION"
}

export interface IPaginacionAction extends Action {
    act_paginacion_numero_res_totales: number;
    act_paginacion_pagina_actual: number;
}