import * as React from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";




interface IDataPickerProps {

    comp_data_picker_valor: Date;
    comp_event_onChange: (fecha: Date) => any;
    comp_nombre_etiqueta: string;
    comp_data_picker_valor_min_estado: null | Date;

}

class DatePickerComponente extends React.Component<IDataPickerProps, {}> {

    constructor(props: IDataPickerProps) {
        super(props);
        this.state = { comp_data_picker_valor_estado: this.props.comp_data_picker_valor};
    }
    
    public com_event_onChange_aux = (valorFecha:Date) => {
        this.props.comp_event_onChange(valorFecha);
    }
    

    

    public render() {
        return (
            <div>
                <label>{this.props.comp_nombre_etiqueta}</label>
                <DatePicker selected={this.props.comp_data_picker_valor} dateFormat='dd/MM/yyyy' 
                            minDate={this.props.comp_data_picker_valor_min_estado} onChange={this.com_event_onChange_aux} />
            </div>
        );
    }
}

export default DatePickerComponente;


