import * as React from 'react';


interface IComboSelectProps {

    comp_options: Array<String>;
    comp_valor: string;
    comp_event_onChange: (valor: string) => any;
    comp_nombre_etiqueta: string;

}

class ComboSelectComponent extends React.Component<IComboSelectProps, {}> {

    constructor(props: IComboSelectProps) {
        super(props);
    }

    public render() {
        return (
            <div>
                <label>{this.props.comp_nombre_etiqueta}</label>
                <select onChange={e => this.props.comp_event_onChange(e.target.value)} value={this.props.comp_valor}>
                    {this.props.comp_options.map(optionAux =>
                        <option value={optionAux.toString()} key={optionAux.toString()}>{optionAux.toString()}</option>
                    )}
                </select>
            </div>
        );
    }
}

export default ComboSelectComponent;


