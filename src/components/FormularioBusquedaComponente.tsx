import * as React from 'react';
import ComboSelectComponente from './ComboSelectComponente';
import CheckboxComponente from './CheckboxComponente';
import DatePickerComponente from './DatePickerComponente'


interface IFormularioBusquedaProps {

    // Propiedades del primer combo con opciones: app web o escritorio
    comp_combo_select_app_web: string;
    comp_combo_select_event_app_web: (opcionSeleccionada: string) => any;
    comp_combo_select_opciones_app_web: Array<string>;

    // Propiedades del segundo combo con opciones: familia
    comp_combo_select_familia: string;
    comp_combo_select_event_familia: (opcionSeleccionada: string) => any;
    comp_combo_select_opciones_familia: Array<string>;

    // Nombre de todas las etiquetas
    comp_nombre_todas_etiquetas: Array<string>;

    // Propiedades del checkbox para indicar solo las apps favoritas
    comp_checkbox_favorito: boolean;
    comp_checkbox_event_favorito: (opcionSeleccionada: boolean) => any;

    // Propiedades del data Picker
    comp_date_picker_fecha_inicio: Date;
    comp_datepicker_event_fecha_ini: (fechaInicio: Date) => any;
    comp_date_picker_fecha_fin: Date;
    comp_datepicker_event_fecha_fin: (fechaFin: Date) => any;

    // Extra: evento para 
    comp_reset_undo_redo: () => void;

}

interface IFormularioBusquedaState {

    // Estados de los componentes del formulario 
    comp_combo_select_app_web_estado: string;
    comp_combo_select_familia_estado: string;

    comp_checkbox_favorito_estado: boolean;

    comp_date_picker_fecha_inicio_estado: Date;
    comp_date_picker_fecha_fin_estado: Date;
}


class FormularioBusquedaComponente extends React.Component<IFormularioBusquedaProps, IFormularioBusquedaState> {

    // Para forzar a que se actualice los componentes
    shouldComponentUpdate(nextProps: IFormularioBusquedaProps, nextState: IFormularioBusquedaState) {

        if (nextProps.comp_combo_select_app_web !== nextState.comp_combo_select_app_web_estado) {
            
            this.setState({comp_combo_select_app_web_estado: nextProps.comp_combo_select_app_web});

        }else if(nextProps.comp_combo_select_familia !== nextState.comp_combo_select_familia_estado){

            this.setState({
                comp_combo_select_familia_estado: nextProps.comp_combo_select_familia});

        }else if(nextProps.comp_checkbox_favorito !== nextState.comp_checkbox_favorito_estado){

            this.setState({
                comp_checkbox_favorito_estado: nextProps.comp_checkbox_favorito});

        }else if(nextProps.comp_date_picker_fecha_inicio.getTime() !== nextState.comp_date_picker_fecha_inicio_estado.getTime()){


            this.setState({
                comp_date_picker_fecha_inicio_estado: nextProps.comp_date_picker_fecha_inicio});

        }else if(nextProps.comp_date_picker_fecha_fin.getTime() !== nextState.comp_date_picker_fecha_fin_estado.getTime()){


            this.setState({
                comp_date_picker_fecha_fin_estado: nextProps.comp_date_picker_fecha_fin});
        }   
        return true;
    }


    constructor(props: IFormularioBusquedaProps) {
        super(props);
        this.state = {
            comp_combo_select_app_web_estado: this.props.comp_combo_select_app_web,
            comp_combo_select_familia_estado: this.props.comp_combo_select_familia,
            comp_checkbox_favorito_estado: this.props.comp_checkbox_favorito,
            comp_date_picker_fecha_inicio_estado: this.props.comp_date_picker_fecha_inicio,
            comp_date_picker_fecha_fin_estado: this.props.comp_date_picker_fecha_fin
        };
    }

    public comp_combo_select_event_app_web_aux = (opcionSeleccionada: string) => {

        this.props.comp_combo_select_event_app_web(opcionSeleccionada);
        this.setState({ comp_combo_select_app_web_estado: opcionSeleccionada });

    }

    public comp_combo_select_event_familia_aux = (opcionSeleccionada: string) => {

        this.props.comp_combo_select_event_familia(opcionSeleccionada);
        this.setState({ comp_combo_select_familia_estado: opcionSeleccionada });

    }

    public comp_combo_checkbox_event_favorito_aux = (opcionSeleccionada: boolean) => {

        this.setState({ comp_checkbox_favorito_estado: opcionSeleccionada });
        this.props.comp_checkbox_event_favorito(opcionSeleccionada);
    }

    public comp_datepicker_event_fecha_ini_aux = (fechaInicio: Date) => {

        let auxIni = new Date(Date.UTC(fechaInicio.getUTCFullYear(), fechaInicio.getMonth(), fechaInicio.getDate()));
        this.props.comp_datepicker_event_fecha_ini(auxIni);
        this.setState({ comp_date_picker_fecha_inicio_estado: auxIni });
    }

    public comp_datepicker_event_fecha_fin_aux = (fechaFin: Date) => {

        let auxFin = new Date(Date.UTC(fechaFin.getUTCFullYear(), fechaFin.getMonth(), fechaFin.getDate()));
        this.props.comp_datepicker_event_fecha_fin(auxFin);
        this.setState({ comp_date_picker_fecha_fin_estado: auxFin });
    }

    public comp_boton_event_reset_campos_aux = () => {

        this.props.comp_combo_select_event_app_web('Cualquiera');
        this.setState({ comp_combo_select_app_web_estado: 'Cualquiera' });

        this.props.comp_combo_select_event_familia('Cualquiera');
        this.setState({ comp_combo_select_familia_estado: 'Cualquiera' });

        this.props.comp_checkbox_event_favorito(false);
        this.setState({ comp_checkbox_favorito_estado: false });

        this.props.comp_datepicker_event_fecha_ini(new Date(Date.UTC(2001, 0, 1)));
        this.setState({ comp_date_picker_fecha_inicio_estado: new Date(Date.UTC(2001, 0, 1)) });

        let auxFin2 = new Date(Date.UTC(new Date().getUTCFullYear(), new Date().getMonth(), new Date().getDate()));
        this.props.comp_datepicker_event_fecha_fin(auxFin2);
        this.setState({ comp_date_picker_fecha_fin_estado: auxFin2 });

        // Extra resetea los valores de redo/undo
        this.props.comp_reset_undo_redo();

    }


    public render() {
        return (
            <div className="buscador_interno js">
                <fieldset>
                    <legend className="sr-only">Buscador</legend>
                    <div className="row">
                        <div className="col-4">
                            <ComboSelectComponente comp_nombre_etiqueta={this.props.comp_nombre_todas_etiquetas[0]}
                                comp_valor={this.state.comp_combo_select_app_web_estado}
                                comp_event_onChange={this.comp_combo_select_event_app_web_aux}
                                comp_options={this.props.comp_combo_select_opciones_app_web} />
                        </div>
                        <div className="col-4">
                            <ComboSelectComponente comp_nombre_etiqueta={this.props.comp_nombre_todas_etiquetas[1]}
                                comp_valor={this.state.comp_combo_select_familia_estado}
                                comp_event_onChange={this.comp_combo_select_event_familia_aux}
                                comp_options={this.props.comp_combo_select_opciones_familia} />
                        </div>
                        <div className="col-4">
                            <CheckboxComponente comp_nombre_etiqueta={this.props.comp_nombre_todas_etiquetas[2]}
                                comp_checked_value={this.state.comp_checkbox_favorito_estado}
                                com_event_onChange={this.comp_combo_checkbox_event_favorito_aux} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4">
                            <DatePickerComponente comp_nombre_etiqueta={this.props.comp_nombre_todas_etiquetas[3]}
                                comp_data_picker_valor={this.state.comp_date_picker_fecha_inicio_estado}
                                comp_event_onChange={this.comp_datepicker_event_fecha_ini_aux}
                                comp_data_picker_valor_min_estado={null} />
                        </div>

                        <div className="col-4">
                            <DatePickerComponente comp_nombre_etiqueta={this.props.comp_nombre_todas_etiquetas[4]}
                                comp_data_picker_valor={this.state.comp_date_picker_fecha_fin_estado}
                                comp_event_onChange={this.comp_datepicker_event_fecha_fin_aux}
                                comp_data_picker_valor_min_estado={this.state.comp_date_picker_fecha_inicio_estado} />
                        </div>
                    </div>
                    <div className="row center">
                        <button className="boton" onClick={this.comp_boton_event_reset_campos_aux}>RESET CAMPOS</button>
                    </div>
                </fieldset>
            </div>
        );
    }
}

export default FormularioBusquedaComponente;