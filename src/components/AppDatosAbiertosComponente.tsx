import * as React from 'react';
import FormularioBusquedaContenedor from '../containers/FormularioBusquedaContenedor';
import PaginacionContenedor from '../containers/PaginacionContenedor';
import InterfaceEnumDatos from '../state/InterfaceEnumDatos';
import { CSSProperties } from 'react';



interface IAplicationDatosAbiertosStyleProps<T>{

    style_img_logo_fondo: T;
    style_p_font_size_titulo:T;
    style_img_float_left:T;
    style_img_float_right:T;
    style_img_margin_auto:T;
    style_text_white:T;
}

// La propiedad de esta clase es una lista de items anteriores. 
interface IAplicacionDatosAbiertosProps {

    comp_lista_objetos: Array<InterfaceEnumDatos>;
    comp_styles: IAplicationDatosAbiertosStyleProps<CSSProperties>;

    comp_app_paginacion_canUndo: boolean;
    comp_app_paginacion_canRedo: boolean;
}



// La clase representa en su contenido cada una de las aplicaciones
class AppDatosAbiertosComponente extends React.Component<IAplicacionDatosAbiertosProps, {}> {
    constructor(props: IAplicacionDatosAbiertosProps) {
        super(props);
        
    }

    public render() {
        return (
            <div id="contenidos">
                <div className="container separacion_vertical_superior_1">
                    <div id="cuerpo" className="grid_16">
                        <h1>Aplicaciones</h1>

                        <FormularioBusquedaContenedor />

                        <noscript>
                            &lt;p&gt;
                            &lt;strong&gt;Esta etiqueta no script se está mostrando porque usted ha desactivado javascript en su navegador.&lt;/strong&gt;
                            &lt;/p&gt;
                        </noscript>

                        <PaginacionContenedor comp_paginacion_canRedo={this.props.comp_app_paginacion_canRedo}
                                              comp_paginacion_canUndo={this.props.comp_app_paginacion_canUndo}/>

                            <ul className="my-3 d-flex">
                                {this.props.comp_lista_objetos.map((item) =>
                                    <div className="row container2 w-auto px-2">
                                        <li>
                                            <div>
                                                { item.destacada===true?
                                                <div style={this.props.comp_styles.style_img_float_left}>
                                                    <img className="m-auto"  src='Junta de Andalucía - Datos abiertos Aplicaciones_files/estrellaFavorito.jpg' alt="" width="40" height="40" />
                                                </div>
                                                :
                                                null
                                                }
                                                { 
                                                
                                                <div className="image2" style={this.props.comp_styles.style_img_logo_fondo}>
                                                    <img className="m-auto"  src={item.srcImg} alt="" width="120" height="120" />
                                                </div>
                                                }
                                                
                                            </div>
                                            { item.titulo.toUpperCase() === 'SIN DATOS'? 
                                            <div>
                                                <div className="prog-desc">
                                                    <p style={this.props.comp_styles.style_p_font_size_titulo} className="center">{item.titulo.toUpperCase()}</p>
                                                    <p></p>
                                                </div>
                                            </div>
                                            :
                                            <div>
                                            <div className="p-2">
                                                <p style={this.props.comp_styles.style_p_font_size_titulo} className="center"><b>{item.titulo.toUpperCase()}</b></p>
                                                <p><b>Descripción:</b> {item.descripcion}</p>
                                                <p><b>Autor:</b>
                                                    <a title={item.autor}>{item.autor}</a>
                                                </p>
                                                <p></p>
                                            </div>
                                            
                                            <div className='overlay'>
                                                <div className="text">
                                                        
                                                        <br></br>
                                                        <br></br>
                                                        <div><b>Fecha publicacion: </b> {item.fechaPublic}</div>
                                                        <div><b>Fuente: </b>{item.fuente}</div>
                                                        <div><b>Precio: </b>{item.precio}</div>
                                                        <br></br> 
                                                        
                                                            {      
                                                            (item.webMovil.indexOf('Web')>-1 ? 
                                                                <div style={this.props.comp_styles.style_img_float_left}>      
                                                                    <img style={this.props.comp_styles.style_img_margin_auto} src='Junta de Andalucía - Datos abiertos Aplicaciones_files/webApp.jpg' alt="" width="30" height="30" />
                                                                    <div> <a href="#" style={this.props.comp_styles.style_text_white}> Web App </a></div>
                                                                </div>  
                                                            :
                                                                null)
                                                            }
                                                        
                                                            {
                                                            (item.webMovil.indexOf('Movil')>-1 ? 
                                                                <div style={this.props.comp_styles.style_img_float_right}>      
                                                                    <img style={this.props.comp_styles.style_img_margin_auto} src='Junta de Andalucía - Datos abiertos Aplicaciones_files/movilApp.png' alt="" width="30" height="30" />
                                                                    <div> <a href="#" style={this.props.comp_styles.style_text_white}>  App Móvil </a> </div>
                                                                </div> 
                                                            :
                                                                null)
                                                            }
                                                </div>
                                            </div>
                                            </div>
                                            }
                                        </li>
                                    </div>
                                )}
                            </ul>
                        <PaginacionContenedor comp_paginacion_canRedo={this.props.comp_app_paginacion_canRedo}
                                               comp_paginacion_canUndo={this.props.comp_app_paginacion_canUndo}/>
                    </div>
                </div>
            </div>


        );
    }
}

export default AppDatosAbiertosComponente;