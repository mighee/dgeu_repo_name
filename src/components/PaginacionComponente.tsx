import * as React from 'react';


interface IPaginacionProps {

    comp_paginacion_num_res_totales: number;
    comp_paginacion_event_onClick: (valor: number) => any;
    comp_paginacion_inicioResultado: number;
    comp_paginacion_finalResultado: number;
    comp_paginacion_num_pagina_actual: number;

    comp_paginacion_undo_event_onClick: () => void;
    comp_paginacion_redo_event_onClick: () => void;

    comp_paginacion_canRedo: boolean;
    comp_paginacion_canUndo: boolean;
}

class PaginacionComponente extends React.Component<IPaginacionProps, {}> {

    constructor(props: IPaginacionProps) {
        super(props);
    }

    public render() {
        return (
            <div className="paginado alpha omega">
                  <ul>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(-10)} href="" title="Inicio" className="corte">
                           <img src="https://www.juntadeandalucia.es/themes/images/paginado_inicio.gif" alt="Inicio" />
                        </a>
                     </li>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(-1)} title="Anterior">
                           <img src="https://www.juntadeandalucia.es/themes/images/paginado_antes.gif" alt="Anterior" />
                        </a>
                     </li>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(1)} title="Ir a página 1">1</a>
                     </li>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(2)} title="Ir a página 2">2</a>
                     </li>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(3)} href="" title="Ir a página 3">3</a>
                     </li>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(4)} href="" title="Ir a página 4">4</a>
                     </li>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(5)} href="" title="Ir a página 5">5</a>
                     </li>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(0)} title="Siguiente" className="corte">
                           <img src="https://www.juntadeandalucia.es/themes/images/paginado_despues.gif" alt="Siguiente" />
                        </a>
                     </li>
                     <li>
                        <a onClick = {e => this.props.comp_paginacion_event_onClick(10)} href="" title="Final" className="corte">
                           <img src="https://www.juntadeandalucia.es/themes/images/paginacion_fin.gif" alt="Final" />
                        </a>
                     </li>
                  </ul>
               <p><b>Pág. número: { this.props.comp_paginacion_num_pagina_actual}.</b> RESULTADOS {this.props.comp_paginacion_inicioResultado} a {this.props.comp_paginacion_finalResultado} de <b>{this.props.comp_paginacion_num_res_totales} Totales</b></p>
               
               {
                  !this.props.comp_paginacion_canRedo?
                     <p style={{backgroundColor:'red'}}>
                        <button onClick= {this.props.comp_paginacion_redo_event_onClick} title="REDO" disabled={!this.props.comp_paginacion_canRedo}>
                           <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/redo.png" width="20" height="20">
                           </img> 
                        </button>      
                     </p> 
                  :
                  <p style={{backgroundColor:'green'}}>
                  <button onClick= {this.props.comp_paginacion_redo_event_onClick} title="REDO" disabled={!this.props.comp_paginacion_canRedo}>
                     <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/redo.png" width="20" height="20">
                     </img> 
                  </button>      
               </p>

               }
               {
                  !this.props.comp_paginacion_canUndo?
                     <p style={{backgroundColor:'red'}}>       
                        <button onClick={this.props.comp_paginacion_undo_event_onClick}  title="UNDO" disabled={!this.props.comp_paginacion_canUndo}>
                           <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/undo.png" width="20" height="20">         
                           </img>
                        </button>
                     </p>
                  :  
                     <p style={{backgroundColor:'green'}}>       
                        <button onClick={this.props.comp_paginacion_undo_event_onClick}  title="UNDO" disabled={!this.props.comp_paginacion_canUndo}>
                           <img src="Junta%20de%20Andaluci%CC%81a%20-%20Datos%20abiertos%20Aplicaciones_files/undo.png" width="20" height="20">         
                           </img>
                        </button>
                     </p>


               }
               </div>
        );
    }


}

export default PaginacionComponente;


