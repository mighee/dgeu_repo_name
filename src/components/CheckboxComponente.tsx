import * as React from 'react';


interface ICheckBoxProps {

    com_event_onChange: (valor: boolean) => any;
    comp_nombre_etiqueta: string;
    comp_checked_value: boolean

}

class CheckboxComponente extends React.Component<ICheckBoxProps, {}> {

    constructor(props: ICheckBoxProps) {
        super(props);
    }

    public render() {

        if(this.props.comp_checked_value === true) {
            return (
                <div>
                        <label><b>{this.props.comp_nombre_etiqueta.toUpperCase()}</b></label>
                        <input type="checkbox" checked style={{marginTop: '1em', marginLeft: '3em', marginRight: '3em'}} onChange={e => this.props.com_event_onChange(e.target.checked)}></input>
                        <img  src="Junta de Andalucía - Datos abiertos Aplicaciones_files/estrellaFavorito.jpg" width="40" height="40" />            
                </div>
            );
        } else {
            return (
                <div>
                    <label><b>{this.props.comp_nombre_etiqueta.toUpperCase()}</b></label>
                    <input type="checkbox" style={{marginTop: "1em", marginLeft: "3em", marginRight: "3em"}} onChange={e => this.props.com_event_onChange(e.target.checked)}></input>
                    <img  src="Junta de Andalucía - Datos abiertos Aplicaciones_files/estrellaFavorito.jpg" width="40" height="40" /> 
                </div>
            );
        }
        
    }
}

export default CheckboxComponente;


